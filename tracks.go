package main

import (
	"github.com/globalsign/mgo/bson"
	"time"
)

/*type TracksStorage interface{
	Init()
	Get(id int) (Track, bool)
	getField(field string, id int) (string, bool)
	Add(t Track) int
	Count() int
}*/

type Track struct {
	H_Date       	time.Time 		`json:"Date"`
	Pilot        	string    		`json:"Pilot"`
	Glider       	string    		`json:"Glider Type"`
	Glider_id    	string    		`json:"Glider ID"`
	Track_length 	float64   		`json:"TrackLength"`
	Track_src_url 	string	  		`json:"TrackSrcUrl"`
	Id              bson.ObjectId	`json:"id"`
}
/*
type TracksInMemoryDB struct {
	tracks map[int]Track
}

func (db *TracksInMemoryDB) Init() {
	db.tracks = make(map[int]Track)
}

func (db *TracksInMemoryDB) Get(id int) (Track, bool) {
	t, ok := db.tracks[id]
	return t, ok
}

func (db *TracksInMemoryDB) getField(field string, id int) (string, bool) {
	valid := true
	r := reflect.ValueOf(db.tracks[id])
	f := reflect.Indirect(r).FieldByName(field)
	// If invalid field
	if f.String() == "<invalid Value>" {
		valid = false
	}
	return string(f.String()), valid
}

func (db *TracksInMemoryDB) Add(t Track) int {
	//Creating a id for the track
	id := rand.Int()

	// Checking if track exists in db
	_, ok := db.Get(id)

	// Creating a new id if it exists
	for ok {
		id = rand.Int()
		_, ok = db.Get(id)
	}

	db.tracks[id] = t

	return id
}

func (db *TracksInMemoryDB) Count() int {
	return len(db.tracks)
}
*/