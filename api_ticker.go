package main

import (
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"net/http"
	"strings"
	"time"
)

type Ticker struct {
	t_latest     int64 			`json:"t_latest"`
	t_start      int64 			`json:"t_start"`
	t_stop       int64 			`json:"t_stop"`
	tracks	     []string	   	`json:"tracks"`
	processing	 time.Duration  `json:"processing"`
}

//Retrieving latest timestamp
func getLatest(w *http.ResponseWriter, db TracksMongoDB) {
	session, err := mgo.Dial(db.DatabaseName)
	if err != nil {
		panic(err)
	}
	defer session.Close()

	//TODO retrieve latest timestamp
}

//retrieving ticker information
func getTicker(w *http.ResponseWriter, db TracksMongoDB) ([]int64) {

	session, err := mgo.Dial(db.DatabaseName)
	if err != nil {
		panic(err)
	}
	defer session.Close()

	var timeStamps []int64
	var all []Track
	err = session.DB(db.DatabaseName).C(db.TracksCollectionName).Find(bson.M{}).All(&all)
	if err != nil {
		return timeStamps
	}

	for _, track := range all {
		timeStamps = append(timeStamps, track.Id.Time().UnixNano()/int64(time.Millisecond))
	}
	return timeStamps
}

func handlerTicker(w http.ResponseWriter, r *http.Request) {

	parts := strings.Split(r.URL.Path, "/")

	if len(parts) == 6 {
		http.Header.Add(w.Header(), "content-type", "text/plain")
	} else {
		http.Header.Add(w.Header(), "content-type", "application/json")
	}

	//General rules
	if parts[1] == "paragliding" && parts[2] != "api" {
		http.Error(w, http.StatusText(404), http.StatusNotFound)
		return
	} else if len(parts) == 5 && parts[3] != "ticker" || len(parts) == 6 && parts[3] != "ticker" {
		http.Error(w, http.StatusText(404), http.StatusNotFound)
		return
	}

	//Decides what is shown according to url and makes sure the url is in correct format
	if len(parts) == 3 && parts[2] == "api" {
		replyWithAPIinformation(&w)
	} else if len(parts) == 4 && parts[3] == "ticker" {
		getTicker(&w, db)
	} else if len(parts) == 5 && parts[4] == "latest" {
		getLatest(&w, db)
	} else if len(parts) == 5 && parts[4] == "timestamp" {
		//TODO create function
	} else {
		http.Error(w, http.StatusText(404), http.StatusNotFound)
		return
	}
}