package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"time"
)

type ApiInformation struct {
	Uptime  string `json:"Uptime"`
	Info    string `json:"Info"`
	Version string `json:"Version"`
}

func replyWithAPIinformation(w *http.ResponseWriter) {
	api.Uptime = isoFormat(serverStart)
	json.NewEncoder(*w).Encode(api)
}

//Uptime formatted according to ISO8601
func isoFormat(startTime time.Time) string {
	newTime := time.Now()
	return fmt.Sprintf("P%dY%dM%dDT%dH%dM%dS",
		newTime.Year()-startTime.Year(), newTime.Month()-startTime.Month(), newTime.Day()-startTime.Day(),
		newTime.Hour()-startTime.Hour(), newTime.Minute()-startTime.Minute(), newTime.Second()-startTime.Second())
}



//--------------------------------------------------------------------------------
var serverStart time.Time
var api = ApiInformation{Uptime: isoFormat(serverStart), Info: "Service for paragliding tracks.", Version: "v1"}
//var db TracksStorage
var IDs []int
var db = TracksMongoDB{
	DatabaseURL: "mongodb://<dbuser>:<dbpassword>@ds145083.mlab.com:45083/paragliding",
	DatabaseName: "paragliding",
	TracksCollectionName: "tracksCollection",
}

//--------------------------------------------------------------------------------

func init() {
	serverStart = time.Now()
}

func main() {
	// in memory
	//db = &TracksInMemoryDB{}

	// mongo persistance
	//db = &TracksMongoDB{}
	db.Init()
	var p string
	if port := os.Getenv("PORT"); port != "" {
		p = ":" + port
	} else {
		p = ":8080"
	}
	http.HandleFunc("/paragliding/", handlerTracks)
	http.HandleFunc("/paragliding/", handlerTicker)
	http.ListenAndServe(p, nil)

}

