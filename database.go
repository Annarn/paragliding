package main

import (
	"fmt"
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"math/rand"
)

//TracksMongoDB stores the details of the DB connection.
type TracksMongoDB struct {
	DatabaseURL 			string
	DatabaseName 			string
	TracksCollectionName 	string
}

/*
Init initializes the mongo storage
 */

func (db *TracksMongoDB) Init() {
	session, err := mgo.Dial(db.DatabaseName)
	if err != nil {
		panic(err)
	}
	defer session.Close()

	// TODO put extra constraints on the track collection
	index := mgo.Index{
		Key: []string{"id"},
		Unique: 	true,
		DropDups: 	true,
		Background: true,
		Sparse: 	true,
	}

	err = session.DB(db.DatabaseName).C(db.TracksCollectionName).EnsureIndex(index)
	if err != nil{
		panic(err)
	}
}

func (db *TracksMongoDB) Get(id int) (Track, bool) {
	session, err := mgo.Dial(db.DatabaseName)
	if err != nil {
		panic(err)
	}
	defer session.Close()

	track := Track{}
	allWasGood := true
	err = session.DB(db.DatabaseName).C(db.TracksCollectionName).Find(bson.M{"id": id}).One(&track)
	if err != nil {
		allWasGood = false
	}

	return track, allWasGood
}

func (db *TracksMongoDB) getField(field string, id int) (string, bool) {
	session, err := mgo.Dial(db.DatabaseName)
	if err != nil {
		panic(err)
	}
	defer session.Close()

	allWasGood := true
	err = session.DB(db.DatabaseName).C(db.TracksCollectionName).Find(nil).Select(bson.M{field: 1}).One(&field)
	if err != nil {
		allWasGood = false
	}
	return field, allWasGood
}

func (db *TracksMongoDB) Add(t Track) int {

	session, err := mgo.Dial(db.DatabaseName)
	if err != nil {
		panic(err)
	}
	defer session.Close()

	//Creating a id for the track
	id := rand.Int()

	// Checking if track exists in db
	_, ok := db.Get(id)

	// Creating a new id if it exists
	for ok {
		id = rand.Int()
		_, ok = db.Get(id)
	}

	err = session.DB(db.DatabaseName).C(db.TracksCollectionName).Insert(t)
	if err!= nil {
		fmt.Printf("error in Insert(): %v", err.Error())
	}
	return id
}

func (db *TracksMongoDB) Count() int {
	session, err := mgo.Dial(db.DatabaseName)
	if err != nil {
		panic(err)
	}
	defer session.Close()

	// handle to "db"
	count, err := session.DB(db.DatabaseName).C(db.TracksCollectionName).Count()
	if err != nil {
		fmt.Printf("error in Count(): %v", err.Error())
		return -1
	}
	return count
}